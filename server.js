// максимально простецкий сервак
// https://nodejs.org/docs/latest/api/http.html
const http = require('node:http');
const server = http.createServer((req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.end('ok');
});

server.listen(3579, () => {
    console.log('server listen. goto http://localhost:' + 3579);
});